const request = require("request");

const forecast = (latitude, longitude, callback) => {
  const url = `https://api.weatherapi.com/v1/current.json?key=2ae8b1ec93f9486d92053007211709&query=${latitude},${longitude}`;
  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback("Unable to connect", undefined);
    } else if (body.error) {
      callback("Unable to find location", undefined);
    } else {
      callback(
        undefined,
        `${body.location.name} has latitude of ${latitude}and longitude of ${longitude} and weather is ${body.current.condition.text} `
      );
    }
  });
};

module.exports = forecast;
